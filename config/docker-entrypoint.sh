#!/usr/bin/env ash
set -e

exec supervisord

if [ -z "${@}" ] && [ "${@}" -ne "supervisord" ]
then
    exec "$@"
fi
